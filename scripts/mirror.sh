#!/bin/sh
APP=${1:?Must specifify a app path}
VERSION=v${2:?Must specicify a version}
NAMESPACE=${3:?Must specify a namespace}

sed -i "s|VERSION_LABEL|${VERSION}|g" ${APP}-deployment.yaml

SUBSETS=$(kubectl get destinationrules.networking.istio.io ${APP} --ignore-not-found -o json --namespace ${NAMESPACE} | jq '.spec.subsets | map(select(.name != "mirrored"))')
if [ "${SUBSETS}" ]; then
    SUBSETS=$(echo $SUBSETS | jq --arg version ${VERSION} '. + [{name:"mirrored",labels:{version:$version}}]')
    yq '.' destination-rule.yaml | jq --argjson subsets "${SUBSETS}" 'setpath(["spec","subsets"];$subsets)' | yq . -y > ${APP}-destination-rule.yaml
    ROUTES=$(kubectl get virtualservices.networking.istio.io ${APP} -o json --namespace ${NAMESPACE} | jq '.spec.http' | jq --arg host "${APP}-svc" 'setpath([0,"mirror"];{host:$host,subset:"mirrored"})')
    yq '.' virtual-service.yaml | jq --argjson route "${ROUTES}" 'setpath(["spec","http"];$route)' | yq . -y > ${APP}-virtual-service.yaml
    sed -i "s|-active|-mirrored|" ${APP}-deployment.yaml
    kubectl delete deployment ${APP}-mirrored --ignore-not-found --namespace ${NAMESPACE}
else
    yq '.' destination-rule.yaml | jq --arg version ${VERSION} 'setpath(["spec","subsets",0];{name:"active",labels:{version:$version}})' | yq . -y > ${APP}-destination-rule.yaml
    yq '.' virtual-service.yaml | jq --arg host "${APP}-svc" 'setpath(["spec","http",0,"route",0];{destination:{host:$host,subset:"active"},weight:100})' | yq . -y > ${APP}-virtual-service.yaml
fi

rm destination-rule.yaml
rm virtual-service.yaml
mkdir k8s
mv *.yaml k8s